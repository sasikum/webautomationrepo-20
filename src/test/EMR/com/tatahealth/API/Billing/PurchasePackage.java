package com.tatahealth.API.Billing;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

/*import com.google.common.collect.Ordering;
import com.google.gson.JsonObject;*/
import com.tatahealth.API.Core.CCAvenueDetails;
import com.tatahealth.API.Core.CommonFunctions;
import com.tatahealth.API.Core.Encryption;

public class PurchasePackage extends Login{
	
	//care package id = 974,975
	// packageType can be "Full Payment" or "Partial Payment"
	public boolean getMyPurchasePackage(String packageType) throws Exception{
		
		url = baseurl + "/Package/api/v1/getMyPurchasePackages/"+consumerUserId;
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		headers.add(new BasicNameValuePair("visitorId","5a14784a-1b50-4757-9968-241a9cdad448"));
		JSONObject responseObj = func.getRequestWithHeaders(url, headers);
		JSONArray packageName = responseObj.getJSONObject("responseData").getJSONArray("packages");
		//System.out.println("packages : "+packageName);
		//System.out.println(packageName.length() + "care package id  "+ ( packageName.get(0)));
		
		JSONObject obj = null;
		Integer pId =0;
		Integer pAmount = 0;
		
		
		for(int i=0;i<packageName.length();i++) {
			obj = (JSONObject) packageName.get(i);
			
			if(obj.getString("packageName").equals(packageType)) {
				//System.out.println("inside if");
				pId = obj.getInt("carePackagesId");
				pAmount = obj.getInt("packageOfferPrice");
			}
		}
		
		packageId = pId.toString();
		packageAmount = pAmount.toString();
		//System.out.println("package id : "+packageId);
		//System.out.println("package amount : "+packageAmount);
		if(pId!=0 && packageId!=null){
			return true;
		}
		return false;
	}
	
	public boolean purchasePackage() throws Exception{
		System.out.println("packageAmount::::"+packageAmount);
		CCAvenueDetails avenueDetails = new CCAvenueDetails();
		avenueDetails.getCCAvenueResponse("package");
		avenueDetails.saveCCAvenueResponse(packageAmount);
		
		url = baseurl + "/Package/api/v2/purchasePackages";
		
		CommonFunctions func = new CommonFunctions();
		List<NameValuePair> headers = new ArrayList<NameValuePair>();
		headers.add(new BasicNameValuePair("userId", consumerUserId));
		headers.add(new BasicNameValuePair("tata_auth_token", consumerToken));
		headers.add(new BasicNameValuePair("misc", userDOB));
		headers.add(new BasicNameValuePair("Content-Type", "text/plain"));
		
		JSONObject param = new JSONObject();
		param.put("consumerUserId", Long.valueOf(consumerUserId));
		param.put("carePackagesId", Long.valueOf(packageId));
		param.put("orderId", orderId);
		param.put("purchaseAmount", Long.valueOf(packageAmount));
		param.put("referenceNo", trackingId);
		param.put("source", "INTERNAL");
		
		String text = param.toString();
		System.out.println("Request for Purchase package:"+text);
		Encryption enc = new Encryption();
		String encKey = "TATADHP-"+userDOB;
		//System.out.println("encryKey : "+encKey);
		String encText = enc.encrypt(text,encKey );
		JSONObject responseObj = func.PostHeaderWithTextRequest(url, encText, headers);
		
		message = responseObj.getJSONObject("status").getString("message");
		System.out.println("Purchase package : "+message);
		
		if(message.equals("Package purchased successfully")){
			return true;
		}
		else{
			return false;
		}
	}
}
