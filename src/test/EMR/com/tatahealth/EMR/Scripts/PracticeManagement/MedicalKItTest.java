
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import java.util.List;


import org.apache.commons.lang3.RandomStringUtils;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.DiagnosisTest;
import com.tatahealth.EMR.Scripts.Consultation.GlobalPrintTest;
import com.tatahealth.EMR.Scripts.Consultation.LabTestModuleTest;
import com.tatahealth.EMR.Scripts.Consultation.PrescriptionTest;
import com.tatahealth.EMR.Scripts.Consultation.SymptomTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;

import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class MedicalKItTest {
	
	public static String kitName = "";
	public static String duplicateKitName ="";
	ExtentTest logger;
	public static String savedGeneralAdviceText ="";
	public static String savedPrescriptions ="";
	public static String savedSymptoms ="";
	public static String savedDiagnosis="";
	public static String savedLabTests ="";
	MedicalKitPages mk = new MedicalKitPages();
	SymptomPage sPage = new SymptomPage();
	SymptomTest sTest = new SymptomTest();
	DiagnosisTest dTest = new DiagnosisTest();
	DiagnosisPage dPage = new DiagnosisPage();
	PrescriptionTest pTest = new PrescriptionTest();
	LabTestPage lPage = new LabTestPage();
	LabTestModuleTest lTest = new LabTestModuleTest();
	PrescriptionPage pPage = new PrescriptionPage();
	VitalMasterTest vmt = new VitalMasterTest();
	AppointmentsPage ap = new AppointmentsPage();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	SymptomPage sp = new SymptomPage();
	GlobalPrintTest gp = new GlobalPrintTest();
	PracticeManagementPages pm = new PracticeManagementPages();
	PracticeManagementTest pmt = new PracticeManagementTest();
	
	@BeforeClass(alwaysRun=true,groups= {"Login","Regression"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement_MedicalKit";
		Login_Doctor.LoginTest();
		new MedicalKItTest().closePreviousConsultations();
		new PracticeManagementTest().moveToPracticeManagement();
	} 
	
	@AfterClass(alwaysRun=true,groups = { "Login","Regression"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
		
		}
	
	//PM_94 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=509)
	public synchronized void moveToMedicalKitAndVerify()throws Exception{
		logger = Reports.extent.createTest("EMR move to medical kit module and verify");
		Web_GeneralFunctions.click(mk.moveToMedicalKitModule(Login_Doctor.driver, logger), "Move to medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMedicalKitDropDown(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getMedicalKitHeader(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getCreateKitButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getMedicalKitDropDown(Login_Doctor.driver, logger)));
		}
	
	//PM_94 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=510)
	public synchronized void validateDisplayOfMedicalKitDropdown() throws Exception {
		logger = Reports.extent.createTest("EMR validate Medical kits dropdown display");
		Web_GeneralFunctions.click(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), "click on medical kit dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		List<WebElement> medicalKits = mk.getAllMedicalkitNamesFromDropdown(Login_Doctor.driver, logger);
		assertTrue(medicalKits!=null);
		
	}
	
	//PM_110 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=511)
	public synchronized void clickOnCreateKitAndVerify() throws Exception {
		logger = Reports.extent.createTest("EMR verify sections in New Medical kit");
		clickCreateKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getUpdateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getSymptomsSection(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getPrescriptionSection(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getSymptomsSection(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getDiagnosisSection(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getLabTestSection(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getGeneralAdviceSection(Login_Doctor.driver, logger)));
	}
	
	//PM_101 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=512)
	public synchronized void saveMedicalKitWithEmptyKitName()throws Exception{
		logger = Reports.extent.createTest("EMR save medical kit with empty kit name");
		clickSaveKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter medical kit name"));
		Web_GeneralFunctions.wait(2);
	}
	
	//PM_102 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=513)
	public synchronized void saveMedicalKitWithEmptyData()throws Exception{
		
		logger = Reports.extent.createTest("EMR save medical kit with empty data");
		kitName = RandomStringUtils.randomAlphabetic(11);
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), kitName, "sending freetext in kit name field", Login_Doctor.driver, logger);
		clickSaveKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.contains("Following field(s) are missing"));
		Web_GeneralFunctions.wait(2);
		
	}
	
	//PM_103 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=514)
	public synchronized void enterSymptomsInMedicalKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter symptom data in Medical Kit");
		//enter in 1st row
		SymptomTest.row = 0;
		Web_GeneralFunctions.clearWebElement(mk.getSymptomTextField(Login_Doctor.driver, SymptomTest.row, logger), "clear symtoms text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String symptomSearchText ="pain";
		Web_GeneralFunctions.sendkeys(mk.getSymptomTextField(Login_Doctor.driver, SymptomTest.row, logger), symptomSearchText, "send symtom search text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(1));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(2));
		List<String> symptomsList = mk.getAllSymptomSuggestionsAsList(Login_Doctor.driver, logger);
		for(String s:symptomsList) {
			assertTrue(s.toLowerCase().contains(symptomSearchText));
		}
		Web_GeneralFunctions.clearWebElement(mk.getSymptomTextField(Login_Doctor.driver, SymptomTest.row, logger), "clear symtoms text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,symptomSearchText,logger), "select symptom from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add new row
		sTest.clickAddSymptoms();
		//2nd row
		SymptomTest.row++;
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,"tio",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add new row
		sTest.clickAddSymptoms();
		SymptomTest.row++;
		String freeText = RandomStringUtils.randomAlphanumeric(12);
		Web_GeneralFunctions.sendkeys(sPage.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), freeText, "sending free text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		assertTrue(mk.getAllSymptomRows(Login_Doctor.driver, logger).size()==3);
		
		
	}
	
	//PM_104 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=515)
	public synchronized void enterDiagnosisInMedicalKit()throws Exception{
		logger = Reports.extent.createTest("EMR enter diagnosis data in Medical Kit");
		DiagnosisTest.row = 0;
		Web_GeneralFunctions.clearWebElement(mk.getDiagnosisTextField(Login_Doctor.driver, DiagnosisTest.row, logger), "clear diagnosis text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String diagnosisSearchText ="salmonella";
		Web_GeneralFunctions.sendkeys(mk.getDiagnosisTextField(Login_Doctor.driver, DiagnosisTest.row, logger), diagnosisSearchText, "send diagnosis search text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(1));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(2));
		List<String> diagnosisList = mk.getAllDiagnosiSuggestionsAsList(Login_Doctor.driver, logger);
		for(String s:diagnosisList) {
			assertTrue(s.toLowerCase().contains(diagnosisSearchText));
		}
		Web_GeneralFunctions.clearWebElement(mk.getDiagnosisTextField(Login_Doctor.driver, DiagnosisTest.row, logger), "clear diagnosis text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		dTest.selectDiagnosis();
		//2nd row diagnosis
		dTest.addDiagnosisNewRowAndSearch();
		dTest.clickAddDiagnosis();
		DiagnosisTest.row++;
		String freeText = RandomStringUtils.randomAlphanumeric(15);
		Web_GeneralFunctions.sendkeys(dPage.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), freeText, "sending free text in diagnosis field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		assertTrue(mk.getAllDiagnosisRows(Login_Doctor.driver,logger).size()==3);
	}
	
	//PM_105 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=516)
	public synchronized void enterPrescription()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter prescription data in Medical Kit");
		PrescriptionTest.row = 0;
		Web_GeneralFunctions.clearWebElement(mk.getPrescriptionTextField(Login_Doctor.driver, PrescriptionTest.row, logger), "clear prescription text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String prescriptionSearchText ="tablet";
		Web_GeneralFunctions.sendkeys(mk.getPrescriptionTextField(Login_Doctor.driver, PrescriptionTest.row, logger), prescriptionSearchText, "send prescription search text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(1));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getAllElementsAsList(Login_Doctor.driver, logger).get(2));
		List<String> prescriptionsList = mk.getAllPrescriptionSuggestionsAsList(Login_Doctor.driver, logger);
		for(String s:prescriptionsList) {
			assertTrue(s.toLowerCase().contains(prescriptionSearchText));
		}
		Web_GeneralFunctions.clearWebElement(mk.getPrescriptionTextField(Login_Doctor.driver, PrescriptionTest.row, logger), "clear prescription text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		pTest.selectMedicine();
		pTest.enterDose();
		pTest.selectDoseUnit();
		pTest.selectFrequnecyFromMasterList();
		pTest.selectRegularlyDurationFromMasterList();
		pTest.enterSplInstruction();
		//add 2nd row
		pTest.addMedicine();
		PrescriptionTest.row++;
		pTest.freeTextInMedicine();
		pTest.enterDuration();
		pTest.addMedicine();
		PrescriptionTest.row++;
		String freeText = RandomStringUtils.randomAlphanumeric(15);
		Web_GeneralFunctions.sendkeys(mk.getPrescriptionTextField(Login_Doctor.driver, PrescriptionTest.row, logger), freeText, "send symtom search text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		pTest.enterDuration();
		pTest.selectFrequnecyFromMasterList();
		Web_GeneralFunctions.wait(1);
		assertTrue(mk.getAllPrescriptionRows(Login_Doctor.driver, logger).size()==3);
	
	}
	
	
	//PM_106 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=517)
	public synchronized void enterGeneralAdvice()throws Exception{
		logger = Reports.extent.createTest("EMR General Advice in Medical Kit");
		savedGeneralAdviceText = RandomStringUtils.randomAlphabetic(40);
		Web_GeneralFunctions.sendkeys(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), savedGeneralAdviceText, "sending free text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getGeneralAdviceDropDown(Login_Doctor.driver, logger), "click general advice drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getGeneralAdviceSearchElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.select1stDropDown(Login_Doctor.driver, logger), "select 1st template from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getAddGeneralAdviceTemplate(Login_Doctor.driver, logger), "click add button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
	}
	
	//PM_106 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=518)
	public synchronized void generalAdviceAddAstemplate()throws Exception{
		
		logger = Reports.extent.createTest("EMR add as template");
		Web_GeneralFunctions.click(mk.getAddAsTemplate(Login_Doctor.driver, logger), "click add as template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getSaveTemplate(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.getSaveTemplate(Login_Doctor.driver, logger), "click save template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String errortext = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(errortext.equalsIgnoreCase("Please enter template name"));
		String specialCharString ="!@#$%^&*({}.<?-_=";
		Web_GeneralFunctions.sendkeys(mk.getGeneralAdviceTemplateName(Login_Doctor.driver, logger),specialCharString,"Sending special character to template Name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(mk.getSaveTemplate(Login_Doctor.driver, logger), "click save template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter template name"));
		String randomTemplateName = RandomStringUtils.randomAlphanumeric(6);
		Web_GeneralFunctions.sendkeys(mk.getGeneralAdviceTemplateName(Login_Doctor.driver, logger),randomTemplateName , "sending template name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(mk.getSaveTemplate(Login_Doctor.driver, logger), "click save template button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("General advice template saved successfully"));
		Web_GeneralFunctions.click(mk.getGeneralAdviceDropDown(Login_Doctor.driver, logger), "click general advice drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getGeneralAdviceSearchElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.sendkeys(mk.getGeneralAdviceSearchElement(Login_Doctor.driver, logger),randomTemplateName , "search with templateName", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getSavedGeneralAdviceElementInDropDown(randomTemplateName, Login_Doctor.driver, logger)));
	}
	
	
	/* lab test with notes is failing due to manual issue, so commenting out the step mean while*/
	//PM_108 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=519)
	public synchronized void enterLabTest()throws Exception{
		logger = Reports.extent.createTest("EMR enter lab test data");
		String atr = Web_GeneralFunctions.getAttribute(mk.getLabTestIdAttribute(Login_Doctor.driver, logger), "id", "get id", Login_Doctor.driver, logger);
		LabTestModuleTest.row = Integer.parseInt(atr);
		lTest.clickAddRow();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please fill all mandatory fields before adding"));
		Web_GeneralFunctions.clearWebElement(mk.getLabTestTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), "clear symtoms text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String labTestSearchText ="blood";
		Web_GeneralFunctions.sendkeys(mk.getLabTestTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), labTestSearchText, "send symtom search text", Login_Doctor.driver, logger);
		List<String> labTestsList = mk.getAllLabTestSuggestionsAsList(Login_Doctor.driver, logger);
		for(String s:labTestsList) {
			assertTrue(s.toLowerCase().contains(labTestSearchText));
		}
		Web_GeneralFunctions.clearWebElement(mk.getLabTestTextField(Login_Doctor.driver, LabTestModuleTest.row, logger), "clear symtoms text field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(lPage.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row, "", logger), "select lab test from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		lTest.clickAddRow();
		LabTestModuleTest.row++;
		lTest.addTestNameWithNotes();
		Web_GeneralFunctions.click(mk.getTestScanRemoveButton(Login_Doctor.driver, LabTestModuleTest.row, logger), "click on Delete Latest row button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=520)
	public synchronized void saveMedicalKitSuccess()throws Exception{
		
		logger = Reports.extent.createTest("EMR save medical kit");
		clickSaveKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getSuccessMessage(Login_Doctor.driver, logger), "save medical kit", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit created successfully."));
		
	}
	
	//PM_95 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=521)
	public synchronized void selectKit()throws Exception{
		logger = Reports.extent.createTest("EMR select saved medical kit");
		Web_GeneralFunctions.click(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), "click on search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getSavedMedicalKit(kitName, Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(mk.getSavedMedicalKit(kitName, Login_Doctor.driver, logger)));
		Web_GeneralFunctions.selectElementByVisibleText(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), kitName, "select saved kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitButton(Login_Doctor.driver, logger));
		List<WebElement> symptomElements = mk.getAllSymptomRows(Login_Doctor.driver, logger);
		for(WebElement symptom:symptomElements) {
			savedSymptoms+=Web_GeneralFunctions.getAttribute(symptom, "value", "extracting symptom text", Login_Doctor.driver, logger);
		}
		List<WebElement> diagnosisElements = mk.getAllDiagnosisRows(Login_Doctor.driver, logger);
		for(WebElement diagnosis :diagnosisElements) {
			savedDiagnosis+=Web_GeneralFunctions.getAttribute(diagnosis, "value", "extracting Diagnosis text", Login_Doctor.driver, logger);
		}
		List<WebElement> prescriptionElements = mk.getAllPrescriptionRows(Login_Doctor.driver, logger);
		for(WebElement prescription :prescriptionElements) {
			savedPrescriptions+=Web_GeneralFunctions.getAttribute(prescription, "value", "extracting prescription text", Login_Doctor.driver, logger);
		}
		savedGeneralAdviceText = Web_GeneralFunctions.getText(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract general advice text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		List<WebElement> labTestElements = mk.getLabTestRows(Login_Doctor.driver, logger);
		for(WebElement tests:labTestElements) {
		savedLabTests+=Web_GeneralFunctions.getAttribute(tests, "value", "extract lab test names", Login_Doctor.driver, logger);
		}
		
		}
	
	//PM_99 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=522)
	public synchronized void updateMedicalKitAndDeleteAddedRow()throws Exception{
		
		logger = Reports.extent.createTest("EMR update medical kit and delete saved row");
		//delete  symptom
		Web_GeneralFunctions.click(sPage.getDeleteButtonAfterSaving(Login_Doctor.driver, SymptomTest.row, logger),"delete last symptom", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//delete  diagnosis
		Web_GeneralFunctions.click(dPage.getDeleteDiagnosis(DiagnosisTest.row, Login_Doctor.driver, logger), "delete diagnosis", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//delete  prescription
		PrescriptionTest.row--;
		Web_GeneralFunctions.click(pPage.getDeleteRow(PrescriptionTest.row, Login_Doctor.driver, logger), "delete prescription row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//delete  lab test
		String atr = Web_GeneralFunctions.getAttribute(mk.getLabTestIdAttribute(Login_Doctor.driver, logger), "id", "get id", Login_Doctor.driver, logger);
		String randomString = RandomStringUtils.randomAlphanumeric(1501);
		Web_GeneralFunctions.clearWebElement(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "clear generalAdvice text area", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.sendkeys(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), randomString, "send random string to general advice", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		LabTestModuleTest.row = Integer.parseInt(atr);
		Web_GeneralFunctions.click(lPage.getDeleteButton(LabTestModuleTest.row, Login_Doctor.driver, logger), "delete lab test", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "click save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit updated successfully."));
		Web_GeneralFunctions.wait(2);
	}
	
	//PM_99 covered here
		@Test(groups= {"Regression","PracticeManagement"},priority=523)
		public synchronized void verifySavedMedicalKit()throws Exception{
			
			logger = Reports.extent.createTest("EMR verify saved medical kit");
			Web_GeneralFunctions.selectElementByVisibleText(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), kitName, "select saved kit name", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitButton(Login_Doctor.driver, logger));
			String updatedGeneralAdviceText = Web_GeneralFunctions.getText(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract general advice text", Login_Doctor.driver, logger);
			
			String updatedSymptoms="";
			String updatedDiagnosis="";
			String updatedPrescription="";
			String updatedLabTests ="";
			List<WebElement> symptomElements = mk.getAllSymptomRows(Login_Doctor.driver, logger);
			for(WebElement symptom:symptomElements) {
				updatedSymptoms+=Web_GeneralFunctions.getAttribute(symptom, "value", "extracting symptom text", Login_Doctor.driver, logger);
			}
			
			List<WebElement> diagnosisElements = mk.getAllDiagnosisRows(Login_Doctor.driver, logger);
			for(WebElement diagnosis :diagnosisElements) {
				updatedDiagnosis+=Web_GeneralFunctions.getAttribute(diagnosis, "value", "extracting Diagnosis text", Login_Doctor.driver, logger);
			}
			
			List<WebElement> prescriptionElements = mk.getAllPrescriptionRows(Login_Doctor.driver, logger);
			for(WebElement prescription :prescriptionElements) {
				updatedPrescription+=Web_GeneralFunctions.getAttribute(prescription, "value", "extracting prescription text", Login_Doctor.driver, logger);
			}
			
			List<WebElement> labTestElements = mk.getLabTestRows(Login_Doctor.driver, logger);
			for(WebElement tests:labTestElements) {
				updatedLabTests+=Web_GeneralFunctions.getAttribute(tests, "value", "extract lab test names", Login_Doctor.driver, logger);
			}
			assertTrue(!savedSymptoms.equals(updatedSymptoms));
			assertTrue(!savedDiagnosis.equals(updatedDiagnosis));
			assertTrue(!savedPrescriptions.equals(updatedPrescription));
			savedSymptoms = updatedSymptoms;
			savedDiagnosis = updatedDiagnosis;
			savedPrescriptions = updatedPrescription;
			savedGeneralAdviceText = updatedGeneralAdviceText;
			savedLabTests = updatedLabTests;
			
		}
	@Test(groups= {"Regression","PracticeManagement"},priority=524)
	public synchronized void createDuplicateMedicalKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR create duplicate medical kit");
		clickCreateKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getUpdateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getSymptomsSection(Login_Doctor.driver, logger));
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), kitName, "sending freetext in kit name field", Login_Doctor.driver, logger);
		//add value in symptom
		SymptomTest.row = 0;
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,"",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add value in diagnosis field
		DiagnosisTest.row = 0;
		dTest.selectDiagnosis();
		//add medicine from template
		Web_GeneralFunctions.click(pPage.getTemplateFromDropDown("", Login_Doctor.driver, logger), "select any one of the template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		//add lab test from template
		Web_GeneralFunctions.click(lPage.getLabTestTemplateFromDropDown("", Login_Doctor.driver, logger), "select lab test template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "click save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit name already exists."));
	}
	
	//PM_98 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=525)
	public synchronized void createAndDeleteKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR create and delete medical kit");
		String name = RandomStringUtils.randomAlphabetic(5);
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		Web_GeneralFunctions.clearWebElement(mk.getKitNameField(Login_Doctor.driver, logger), "clear medical kit name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), name, "sending free text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		pmt.clickOrgHeader();
		pmt.moveToPracticeManagement();
		Web_GeneralFunctions.click(mk.moveToMedicalKitModule(Login_Doctor.driver, logger), "Move to medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMedicalKitDropDown(Login_Doctor.driver, logger));
		String kit = kitName;
		kitName = name;
		Web_GeneralFunctions.selectElementByVisibleText(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), kitName, "select saved kit name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitButton(Login_Doctor.driver, logger));
		kitName = kit;
		clickDeleteKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitPopUp(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getDeleteKitPopUp(Login_Doctor.driver, logger), "get pop up message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Do you really want to delete this kit?"));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=526)
	public synchronized void clickNoInDeleteKitPopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR click no in delete kit pop up");
		Web_GeneralFunctions.click(mk.getDeleteKitNoButton(Login_Doctor.driver, logger), "click no in medical kit pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitButton(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=527)
	public synchronized void clickYesInDeleteKitPopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR click yes in delete kit pop up");
		clickDeleteKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.getDeleteKitYesButton(Login_Doctor.driver, logger), "click yes in medical kit pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit deleted successfully."));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=528)
	public synchronized void applyMedicalKitInConsultationPage()throws Exception{
		
		logger = Reports.extent.createTest("EMR click yes in delete kit pop up");
		PracticeManagementTest pmt = new PracticeManagementTest();
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(mk.getSelectMedicalKit(kitName, Login_Doctor.driver, logger), "select medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getApplyMedicalKit(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.getApplyMedicalKit(Login_Doctor.driver, logger), "click apply medical kit button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get success message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit applied successfully"));
		Web_GeneralFunctions.wait(3);	
		Web_GeneralFunctions.scrollUp(Login_Doctor.driver);
		String symptomsInConsultationPage="";
		String diagnosisInConsultationPage="";
		String prescriptionInConsultationPage="";
		String labTestsInConsultationPage ="";
		String generalAdviceTextInConsultationPage ="";
		List<WebElement> symptomElements = mk.getAllSymptomRows(Login_Doctor.driver, logger);
		for(WebElement symptom:symptomElements) {
			symptomsInConsultationPage+=Web_GeneralFunctions.getAttribute(symptom, "value", "extracting symptom text", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.scrollElementToCenter(mk.getDiagnosisHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(2);
		List<WebElement> diagnosisElements = mk.getAllDiagnosisRows(Login_Doctor.driver, logger);
		for(WebElement diagnosis :diagnosisElements) {
			diagnosisInConsultationPage+=Web_GeneralFunctions.getAttribute(diagnosis, "value", "extracting Diagnosis text", Login_Doctor.driver, logger);
		}
		
		Web_GeneralFunctions.scrollElementToCenter(mk.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(2);
		List<WebElement> prescriptionElements = mk.getAllPrescriptionRows(Login_Doctor.driver, logger);
		for(WebElement prescription :prescriptionElements) {
			prescriptionInConsultationPage+=Web_GeneralFunctions.getAttribute(prescription, "value", "extracting prescription text", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.scrollElementToCenter(mk.getLabTestHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(2);
		List<WebElement> labTestElements = mk.getLabTestRows(Login_Doctor.driver, logger);
		for(WebElement tests:labTestElements) {
			labTestsInConsultationPage+=Web_GeneralFunctions.getAttribute(tests, "value", "extract lab test names", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.scrollElementToCenter(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
		generalAdviceTextInConsultationPage = Web_GeneralFunctions.getText(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract general advice text", Login_Doctor.driver, logger);
		backToMedicalKit();
		Web_GeneralFunctions.selectElementByVisibleText(mk.getMedicalKitDropDown(Login_Doctor.driver, logger), kitName, "select saved kit name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitButton(Login_Doctor.driver, logger));
		clickDeleteKit();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getDeleteKitPopUp(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.getDeleteKitYesButton(Login_Doctor.driver, logger), "click yes in medical kit pop up", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		assertTrue(savedSymptoms.equals(symptomsInConsultationPage));
		assertTrue(savedDiagnosis.equals(diagnosisInConsultationPage));
		assertTrue(savedPrescriptions.equals(prescriptionInConsultationPage));
		assertTrue(savedLabTests.equals(labTestsInConsultationPage));
		assertTrue(savedGeneralAdviceText.equals(generalAdviceTextInConsultationPage));
		
	}
	
	public synchronized void backToMedicalKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR Back to Medical kit from consultation page");
		pmt.moveToPracticeManagement();
		moveToMedicalKitAndVerify();
	}
	
	public synchronized void clickCreateKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR click create kit");
		Web_GeneralFunctions.click(mk.getCreateKitButton(Login_Doctor.driver, logger), "Click create kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getUpdateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getKitNameField(Login_Doctor.driver, logger));
		
		
	}
	
	public synchronized void clickSaveKit()throws Exception{
		logger = Reports.extent.createTest("EMR click save kit");
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "Click save kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	public synchronized void clickUpdateKit()throws Exception{
		logger = Reports.extent.createTest("EMR click update kit");
		MedicalKitPages mk = new MedicalKitPages();
		Web_GeneralFunctions.click(mk.getUpdateKitButton(Login_Doctor.driver, logger), "Click update kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	public synchronized void clickDeleteKit()throws Exception{
		
		logger = Reports.extent.createTest("EMR click delete kit");
		Web_GeneralFunctions.click(mk.getDeleteKitButton(Login_Doctor.driver, logger), "Click delete kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		
	}
	
	
	public synchronized void closePreviousConsultations() throws Exception {
		logger = Reports.extent.createTest("EMR close previous consultations");
		Web_GeneralFunctions.click(pm.getLeftMainMenu(Login_Doctor.driver, logger), "Click Left main menu ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getPracticeManagementMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getOpenConsultationsModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(mk.getOpenConsultationsModule(Login_Doctor.driver, logger), "click on open Consultations", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getPatientSearchButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getPatientIdSearch(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(mk.getPatientSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
			if(!Web_GeneralFunctions.isVisible(mk.getNoOpenConsultationsText(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				List<WebElement> openConsultationCheckoutElements = mk.getAllCheckoutElements(Login_Doctor.driver, logger);
				for(WebElement checkout:openConsultationCheckoutElements) {
					Web_GeneralFunctions.click(checkout, "click on checkout link", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(mk.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getPatientSearchButton(Login_Doctor.driver, logger));
				}
			assertTrue(Web_GeneralFunctions.isDisplayed(mk.getNoOpenConsultationsText(Login_Doctor.driver, logger)));
			
			}
		
		
	}
	
}

