
package com.tatahealth.EMR.Scripts.PracticeManagement;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;


import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.EMR.Scripts.Consultation.ConsultationTest;
import com.tatahealth.EMR.Scripts.Consultation.DiagnosisTest;

import com.tatahealth.EMR.Scripts.Consultation.SymptomTest;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;

import com.tatahealth.EMR.pages.PracticeManagement.GeneralAdviceTemplatePages;
import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;

import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.Web_Testbase;

public class GeneralAdviceTemplateTest {
	
	public static String templateName = "";
	public static String templateData ="";
	VitalMasterTest vmt = new VitalMasterTest();
	DiagnosisPage dp = new DiagnosisPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	SymptomPage sp = new SymptomPage();
	MedicalKItTest mkt = new MedicalKItTest();
	MedicalKitPages mk = new MedicalKitPages();
	SymptomPage sPage = new SymptomPage();
	DiagnosisTest dTest = new DiagnosisTest();
	PrescriptionPage pPage = new PrescriptionPage();
	LabTestPage lPage = new LabTestPage();
	PracticeManagementTest pmt = new PracticeManagementTest();
	GeneralAdviceTemplatePages gat = new GeneralAdviceTemplatePages();
	ExtentTest logger;
	

	@BeforeClass(alwaysRun=true,groups= {"Regression"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR_PracticeManagement_GeneralAdvice";
		Login_Doctor.LoginTest();
		new MedicalKItTest().closePreviousConsultations();
		new PracticeManagementTest().moveToPracticeManagement();
	} 
	
	@AfterClass(alwaysRun=true,groups = {"Regression"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
		
	}
	
	//PM_116 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=544)
	public synchronized void moveToGeneralAdviceMenuAndVerify(){
		logger = Reports.extent.createTest("EMR move to General advice template");
		assertTrue(Web_GeneralFunctions.isDisplayed(gat.moveToGeneralAdviceTemplateModule(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(gat.moveToGeneralAdviceTemplateModule(Login_Doctor.driver, logger), "Move to General advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getCreateTemplate(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getDeleteButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getSaveButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=545)
	public synchronized void saveGeneralAdviceTemplateWithoutData()throws Exception{
		logger = Reports.extent.createTest("EMR save General advice template without data");
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please select a template"));
		Web_GeneralFunctions.wait(5);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=546)
	public synchronized void deleteGeneralAdviceTemplateWithoutData()throws Exception{
		logger = Reports.extent.createTest("EMR delete General advice template without data");
		clickDeleteGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Select a general advice template"));
		Web_GeneralFunctions.wait(5);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=547)
	public synchronized void saveTemplateWithEmptyTemplateName()throws Exception{
		logger = Reports.extent.createTest("EMR save template with emtpy template name");
		clickCreateGeneralAdviceTemplate();
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter template name"));
		Web_GeneralFunctions.wait(5);
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=548)
	public synchronized void saveTemplateWithEmptyTemplateData()throws Exception{
		logger = Reports.extent.createTest("EMR save template with emtpy template name");
		templateName = RandomStringUtils.randomAlphabetic(7);
		Web_GeneralFunctions.sendkeys(gat.getCreatetemplateName(Login_Doctor.driver, logger), templateName, "Sending free text in template name", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Please enter template data"));
		Web_GeneralFunctions.wait(4);
	}
	
	//PM_117 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=549)
	public synchronized void saveTemplateSuccess()throws Exception{
		logger = Reports.extent.createTest("EMR save template with valid data");
		templateData = RandomStringUtils.randomAlphabetic(50);
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), templateData, "Sending free text in template name", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("General advice template saved successfully"));
	}
	
	//PM_123 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=550)
	public synchronized void verifyDispalyOfSavedGeneralAdviceTemplateInDropdown() throws Exception {
		logger = Reports.extent.createTest("EMR verify saved template Name in dropdown suggestions");
		Web_GeneralFunctions.click(gat.getSelectGeneralAdviceTemplate(Login_Doctor.driver, logger), "click on dropdown element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		List<String> templateNames = gat.getAllGeneralAdviceTemplatesNamesAslist(Login_Doctor.driver, logger);
		for(int i=0;;i++) {
			if(i<templateNames.size()) {
				if(templateNames.get(i).equals(templateName) ) {
				assertTrue(true);
				break;
			}
		}else
			assertTrue(false);
	}
	}
	
	//PM_118 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=551)
	public synchronized void selectTheSavedTemplateAndUpdate()throws Exception{
		logger = Reports.extent.createTest("EMR select saved template and edit and save the changes");
		templateData = RandomStringUtils.randomAlphabetic(1501);
		Web_GeneralFunctions.selectElementByVisibleText(gat.getSelectGeneralAdviceTemplate(Login_Doctor.driver, logger), templateName, "get saved general advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.clearWebElement(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "clear general advice", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), templateData, "Sending free text in template name", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("General advice template saved successfully"));
	}
	
	//PM_120 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=552)
	public synchronized void createDuplicateTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR create duplicate template");
		clickCreateGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getCreatetemplateName(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,gat.getSelectTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.sendkeys(gat.getCreatetemplateName(Login_Doctor.driver, logger), templateName, "sending existing template name", Login_Doctor.driver, logger);
		String templateTextData = RandomStringUtils.randomAlphabetic(20);
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), templateTextData, "sending template data", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get Error message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("General advice template exists already"));
	}
	
	//PM_121, PM_122 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=553)
	public synchronized void createTemplateWithMoreThan150Char()throws Exception{
		logger = Reports.extent.createTest("EMR create template with more than 150 characters");
		Web_GeneralFunctions.clearWebElement(gat.getCreatetemplateName(Login_Doctor.driver, logger), "clear the template name", Login_Doctor.driver, logger);
		templateName = RandomStringUtils.randomAlphabetic(151);
		Web_GeneralFunctions.sendkeys(gat.getCreatetemplateName(Login_Doctor.driver, logger), templateName, "sending existing template name", Login_Doctor.driver, logger);
		String templateTextData = RandomStringUtils.randomAlphabetic(20);
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), templateTextData, "sending template data", Login_Doctor.driver, logger);
		String templateNameLength = Web_GeneralFunctions.getAttribute(gat.getCreatetemplateName(Login_Doctor.driver, logger), "maxlength", "get max length", Login_Doctor.driver, logger);
		String templateDataLength = Web_GeneralFunctions.getAttribute(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "maxlength", "get max length", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		assertTrue(templateNameLength.equalsIgnoreCase("150") && templateDataLength.equalsIgnoreCase("1500"));
	}
	
	//PM_119 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=554)
	public synchronized void deleteTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR delete template");
		templateName = templateName.substring(0, templateName.length()-1);
		Web_GeneralFunctions.selectElementByVisibleText(gat.getSelectGeneralAdviceTemplate(Login_Doctor.driver, logger), templateName, "select template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		clickDeleteGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gat.getSelectGeneralAdviceTemplate(Login_Doctor.driver, logger), "click on dropdown element", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		List<String> templateNames = gat.getAllGeneralAdviceTemplatesNamesAslist(Login_Doctor.driver, logger);
		for(String name :templateNames) {
			if(name.equals(templateName)) {
				assertTrue(false);
			}
		}
		assertTrue(text.equalsIgnoreCase("General advice template deleted successfully"));
	}
	
	@Test(groups= {"Regression","PracticeManagement"},priority=555)
	public synchronized void ApplyAndValidateGeneralAdviceTemplateInConsultationPage()throws Exception{
		logger = Reports.extent.createTest("EMR Apply saved General Advice template in Consultation");
		clickCreateGeneralAdviceTemplate();
		templateName = RandomStringUtils.randomAlphabetic(15);
		Web_GeneralFunctions.sendkeys(gat.getCreatetemplateName(Login_Doctor.driver, logger), templateName, "sending existing template name", Login_Doctor.driver, logger);
		templateData = RandomStringUtils.randomAlphabetic(20);
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTextArea(Login_Doctor.driver, logger), templateData, "sending template data", Login_Doctor.driver, logger);
		clickSaveGeneralAdviceTemplate();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(gat.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("General advice template saved successfully"));
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".patientUHID");
		pmt.clickOrgHeader();
		vmt.startConsultation();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.moveToDiagnosisModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger) );
		selectSavedGeneralAdviceTemplateInConsultation();
	}
	
	//PM_125 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=556)
	public synchronized void saveMedicalKitwithOutGeneralAdvice() throws Exception {
		logger = Reports.extent.createTest("EMR save medical kit with empty generalAdvice");
		pmt.moveToPracticeManagement();
		Web_GeneralFunctions.click(mk.moveToMedicalKitModule(Login_Doctor.driver, logger), "Move to medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMedicalKitDropDown(Login_Doctor.driver, logger));
		mkt.clickCreateKit();
		MedicalKItTest.kitName =RandomStringUtils.randomAlphabetic(7);
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), MedicalKItTest.kitName, "sending freetext in kit name field", Login_Doctor.driver, logger);
		SymptomTest.row = 0;
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,"",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		DiagnosisTest.row = 0;
		dTest.selectDiagnosis();
		Web_GeneralFunctions.scrollElementToCenter(mk.getPrescriptionTemplateSearch(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(pPage.getTemplateFromDropDown("", Login_Doctor.driver, logger), "select any one of the template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollToElement(mk.getLabTestTemplateSearch(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getLabTestTemplateSearch(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(lPage.getLabTestTemplateFromDropDown("", Login_Doctor.driver, logger), "select lab test template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "click save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit created successfully."));
	}
	
	//PM_124 covered here
	@Test(groups= {"Regression","PracticeManagement"},priority=557)
	public synchronized void saveMedicalKitwithGeneralAdvice() throws Exception {
		logger = Reports.extent.createTest("EMR save medical kit with generalAdvice");
		Web_GeneralFunctions.click(mk.moveToMedicalKitModule(Login_Doctor.driver, logger), "Move to medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getCreateKitButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMedicalKitDropDown(Login_Doctor.driver, logger));
		mkt.clickCreateKit();
		MedicalKItTest.kitName =RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(mk.getKitNameField(Login_Doctor.driver, logger), MedicalKItTest.kitName, "sending freetext in kit name field", Login_Doctor.driver, logger);
		SymptomTest.row = 0;
		Web_GeneralFunctions.click(sPage.selectSymptomFromDropDown(Login_Doctor.driver,SymptomTest.row,"",logger), "select symptom from drop down", Login_Doctor.driver, logger);
		DiagnosisTest.row = 0;
		dTest.selectDiagnosis();
		Web_GeneralFunctions.scrollElementToCenter(mk.getPrescriptionTemplateSearch(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(pPage.getTemplateFromDropDown("", Login_Doctor.driver, logger), "select any one of the template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(mk.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(mk.getGeneralAdviceDropDown(Login_Doctor.driver, logger), "click general advice drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getGeneralAdviceSearchElement(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(mk.select1stDropDown(Login_Doctor.driver, logger), "select 1st template from drop down", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(mk.getAddGeneralAdviceTemplate(Login_Doctor.driver, logger), "click add button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.scrollToElement(mk.getLabTestTemplateSearch(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getLabTestTemplateSearch(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(lPage.getLabTestTemplateFromDropDown("", Login_Doctor.driver, logger), "select lab test template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(mk.getSaveKitButton(Login_Doctor.driver, logger), "click save medical kit", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, mk.getMessage(Login_Doctor.driver, logger));
		String text = Web_GeneralFunctions.getText(mk.getMessage(Login_Doctor.driver, logger), "get message", Login_Doctor.driver, logger);
		assertTrue(text.equalsIgnoreCase("Medical kit created successfully."));
	}
		
	public synchronized void clickSaveGeneralAdviceTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR click save General advice template");
		Web_GeneralFunctions.click(gat.getSaveButton(Login_Doctor.driver, logger), "click save General advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
	}
	
	public synchronized void selectSavedGeneralAdviceTemplateInConsultation() throws Exception {
		logger = Reports.extent.createTest("EMR Selecting saved General Advice template in consulation");
		GeneralAdvicePage advicepage = new GeneralAdvicePage();
		Web_GeneralFunctions.scrollElementToCenter(gat.getGeneralAdviceHeader(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, advicepage.generalAdviceTemplateDropDown(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(advicepage.generalAdviceTemplateDropDown(Login_Doctor.driver, logger), "click on dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getGeneralAdviceTemplateSearch(Login_Doctor.driver, logger));
		Web_GeneralFunctions.sendkeys(gat.getGeneralAdviceTemplateSearch(Login_Doctor.driver, logger), templateName, "searching for GA template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gat.getFirstGeneralAdviceTemplate(templateName, Login_Doctor.driver, logger), "selecting General Advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(advicepage.getAddButton(Login_Doctor.driver, logger), "Adding the General Adivce template", Login_Doctor.driver,logger);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(gat.getGeneralAdviceHeader(Login_Doctor.driver, logger), Login_Doctor.driver);
		String text = Web_GeneralFunctions.getText(advicepage.getGeneralAdviceTextArea(Login_Doctor.driver, logger),"getting text inside text area", Login_Doctor.driver, logger);
		assertTrue(text.contains(templateData));
	}
	
	public synchronized void clickDeleteGeneralAdviceTemplate(){
		logger = Reports.extent.createTest("EMR click delete General advice template");
		Web_GeneralFunctions.click(gat.getDeleteButton(Login_Doctor.driver, logger), "click delete General advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getMessage(Login_Doctor.driver, logger));
	}
	
	public synchronized void clickCreateGeneralAdviceTemplate()throws Exception{
		logger = Reports.extent.createTest("EMR click create General advice template");
		Web_GeneralFunctions.click(gat.getCreateTemplate(Login_Doctor.driver, logger), "click create General advice template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getSelectTemplateButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gat.getSaveButton(Login_Doctor.driver, logger));
	}
	
}

