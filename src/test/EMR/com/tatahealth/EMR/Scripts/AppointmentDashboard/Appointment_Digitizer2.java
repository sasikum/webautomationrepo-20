package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Digitizer;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.reusableModules;

public class Appointment_Digitizer2 {
	
	ExtentTest logger;
	
	
	@BeforeClass(groups= {"Regression","Appointment_Digitizer"})
	public static void beforeClass() throws Exception {
		Login_Digitizer.LoginTest();
		System.out.println("Initialized Driver");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Digitizer"})
	public static void afterClass() throws Exception {
		Login_Digitizer.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check appointment time scroll function - Booked appointment for shown slot
	 */
	@Test(priority=56,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_006() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_010");
		
		AllSlotsPage ASPage = new AllSlotsPage();
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("mm");
		//String localDate = date.toString().substring(8, 10);
		
		//Moving to All slots Page
		Thread.sleep(10000);
		int n = Login_Digitizer.driver.findElements(By.className("appointmentTimeScroll")).size();
		Thread.sleep(10000);
		//System.out.println(n);
		
		//Checking and Printing time
		String SlotTime = ASPage.getTimeforSelectedSlot(n+1, Login_Digitizer.driver, logger);
		int SlotTimeMin = Integer.parseInt(SlotTime.substring((SlotTime.indexOf(":")+1)));
		int CurrentTime = Integer.parseInt(df.format(date));
		System.out.println(SlotTimeMin);
		System.out.println(CurrentTime);
		
		if(SlotTimeMin-CurrentTime>=0||SlotTimeMin-CurrentTime<=-50) {
			System.out.println("Scrolled Correctly");
		}
		
	
	}
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Blocked Slot check - All Cases
	 * Counts - All Cases
	 */
	@Test(priority=57,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_007() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_011");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		ConsultationPage ConsPage = new ConsultationPage();
		
		//Moving to All slots Page
		Thread.sleep(5000);
		int n = Login_Digitizer.driver.findElements(By.className("appointmentTimeScroll")).size();
		int appCnt = 0;
		
		//Booking Appointment For Reschedule
		int BA_R = n+6;
		String slotId = ASpage.getSlotId(BA_R, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		String StartSlotTime = ASpage.getTimeforSelectedSlot(BA_R, Login_Digitizer.driver, logger);
		String StartType = ASpage.getAMPMforSelectedSlot(BA_R, Login_Digitizer.driver, logger);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		
		
		
		//Booking Appointment For Cancel
		int BA_C = BA_R+1;
		slotId = ASpage.getSlotId(BA_C, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		
		
		//Booking Appointment & Checkin 
		int BA_CI = BA_C+1;
		slotId = ASpage.getSlotId(BA_CI, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		int CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, Login_Digitizer.driver, logger), "Clicking on Checkin in Dropdown", Login_Digitizer.driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(6000);
		
		
		//Booking Appointment & Checkin & Preconsulting
		int BA_CI_P_ing = BA_CI+1;
		slotId = ASpage.getSlotId(BA_CI_P_ing, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(5000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Booking Appointment & Checkin & Preconsulted
		int BA_CI_P_ed = BA_CI_P_ing+1;
		slotId = ASpage.getSlotId(BA_CI_P_ed, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		
		//Book Appointment and Consulting
		int BA_C_ing = BA_CI_P_ed+1;
		slotId = ASpage.getSlotId(BA_C_ing, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,3, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		
		//Book Appointment & Consulted
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		int BA_C_ed = BA_C_ing+1;
		slotId = ASpage.getSlotId(BA_C_ed, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		String EndSlotTime = ASpage.getTimeforSelectedSlot(BA_C_ed+1, Login_Digitizer.driver, logger);
		String EndType = ASpage.getAMPMforSelectedSlot(BA_C_ed+1, Login_Digitizer.driver, logger);
		appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		int CheckOutCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,3, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		CompletedCountCheck(CheckOutCnt, true);
		int newCheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		if(newCheckinCnt==CheckinCnt+1) {
			System.out.println("Working Fine");
		}
		
		
		//Now main (p)art : Blocking the slots
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String date = df.format(new Date());
		
		String BlockStartTime = date+" "+StartSlotTime+" "+StartType;
		String BlockEndTime = date+" "+EndSlotTime+" "+EndType;
		
		Web_GeneralFunctions.click(ASpage.getBlockSlotIcon(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		
		Web_GeneralFunctions.clear(ASpage.setStartTime(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setStartTime(Login_Digitizer.driver, logger), BlockStartTime, "dummy", Login_Digitizer.driver, logger);
		
		Web_GeneralFunctions.clear(ASpage.setEndTime(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setEndTime(Login_Digitizer.driver, logger), BlockEndTime, "dummy", Login_Digitizer.driver, logger);
		
		Web_GeneralFunctions.click(ASpage.getBlockReason(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setBlockReason(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBlockSlotSubmitReason(Login_Digitizer.driver, logger), "dummy", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		
		
		
		
		
		//Now Clap for Checks after blocking slot
		//Rescheduling First Booked Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_R, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_R, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_R,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking to get timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((BA_R+2), Login_Digitizer.driver, logger), "Clicking to select next slot timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking on Ok in Popup", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		
		//Cancelling Second Booked Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C,2, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Digitizer.driver, logger), "Clicking to get reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Digitizer.driver, logger),"Clicking on reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Digitizer.driver, logger), "Clicking on Submit in Alert", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		
		
		//Cancelling Checked in Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Digitizer.driver, logger), "Clicking to get reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Digitizer.driver, logger),"Clicking on reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Digitizer.driver, logger), "Clicking on Submit in Alert", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		
		
		//Preconsulting check
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI_P_ing, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		
		//Preconsulted check
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI_P_ed, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		
		//Consulting Check
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ed, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		
		//Consulted Check
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ing, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Thread.sleep(6000);
		
		
		
		
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booking Appointments today for past date check tomorrow
	 */
	@Test(priority=58,groups= {"Regression","Appointment_Digitizer_PastDate"})
	public synchronized void APP_DIGI_008() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_012");
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		AppointmentsPage appoiPage = new AppointmentsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		ConsultationPage ConsPage = new ConsultationPage();
		
		//Moving to All slots Page
		
		Thread.sleep(5000);
		int n = Login_Digitizer.driver.findElements(By.className("applist")).size();
		
		//Booking Appointment
		int BA_R = n-5;
		String slotId = ASpage.getSlotId(BA_R, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Booking Appointment & Checkin 
		int BA_CI = BA_R+1;
		slotId = ASpage.getSlotId(BA_CI, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		int CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, Login_Digitizer.driver, logger), "Clicking on Checkin in Dropdown", Login_Digitizer.driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(6000);
		
		
		//Booking Appointment & Checkin & Preconsulting
		int BA_CI_P_ing = BA_CI+1;
		slotId = ASpage.getSlotId(BA_CI_P_ing, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(5000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		Thread.sleep(6000);
		
		//Booking Appointment & Checkin & Preconsulted
		int BA_CI_P_ed = BA_CI_P_ing+1;
		slotId = ASpage.getSlotId(BA_CI_P_ed, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		
		//Book Appointment and Consulting
		int BA_C_ing = BA_CI_P_ed+1;
		slotId = ASpage.getSlotId(BA_C_ing, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,3, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		
		//Book Appointment & Consulted
		CheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		int BA_C_ed = BA_C_ing+1;
		slotId = ASpage.getSlotId(BA_C_ed, Login_Digitizer.driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		int CheckOutCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,3, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		Thread.sleep(6000);
		CompletedCountCheck(CheckOutCnt, true);
		int newCheckinCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		if(newCheckinCnt==CheckinCnt+1) {
			System.out.println("Working Fine");
		}
		
	
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check past appointments
	 */
	@Test(priority=99,groups= {"Regression","Appointment_Digitizer_PastDate"})
	public synchronized void APP_DIGI_009() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_013");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		Login_Digitizer.driver.findElement(By.cssSelector("#inputDate")).click();
		Thread.sleep(2000);
		
		int week = reusableModules.getcurrentWeekfromCalendar();
		int n = reusableModules.getcurrentDayfromCalendar(Login_Digitizer.driver);
		
		if(week==1&&n==1) {
			week = 7;
			n = 5;
			
		}else if(week==1){
			week = 7;
			n = n-1;
		}else {
			week = week-1;
		}
		
		Login_Digitizer.driver.findElement(By.cssSelector("#inputDate")).click();
		if(week==7&&n==5) {
			Web_GeneralFunctions.click(ASpage.getPrevMonthBtn(Login_Digitizer.driver, logger), "Clicking to go to previous month", Login_Digitizer.driver, logger);
		}
		Web_GeneralFunctions.click(ASpage.getDatetoClick(n, week, Login_Digitizer.driver, logger), "Clicking on previous date", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		int slotsCnt = Login_Digitizer.driver.findElements(By.className("appointmentTimeScroll")).size();
		System.out.println(slotsCnt);
		
		int BA_R = slotsCnt-5;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_R, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_R, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_R,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		//Checked In
		int BA_CI = BA_R+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		//Preconsulting check
		int BA_CI_P_ing = BA_CI+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(6000);
		
		
		//Consulted Check
		int BA_C_ed = BA_CI_P_ing+2;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ed, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
		Thread.sleep(6000);
		
		
		//Consulted Check
		int BA_C_ing = BA_C_ed+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ing, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		Thread.sleep(6000);
		
		//preconsulted check
		int BA_CI_P_ed = BA_CI_P_ing+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
	
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Appointment Booking - Reschedule and Cancel - Future Date
	 */
	@Test(priority=64,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_DIGI_010() throws Exception {
		
		logger = Reports.extent.createTest("APP_DIGI_014");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		Thread.sleep(2000);
		Login_Digitizer.driver.findElement(By.cssSelector("#inputDate")).click();
		Thread.sleep(2000);
		
		int week = reusableModules.getcurrentWeekfromCalendar();
		int n = reusableModules.getcurrentDayfromCalendar(Login_Digitizer.driver);
		
		if(week==7) {
			week = 1;
			n = n+1;
		}else {
			week = week+1;
		}
		
		
		Web_GeneralFunctions.click(ASpage.getDatetoClick(n, week, Login_Digitizer.driver, logger), "Clicking on Future date", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		n = ASpage.getAvailableSlot(Login_Digitizer.driver, logger);
		System.out.println(n);
		String slotId = ASpage.getSlotId(n, Login_Digitizer.driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
				
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
				
				
		//Rescheduling appointment to next slot
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(2000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 1, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Thread.sleep(2000);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking to get timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), Login_Digitizer.driver, logger), "Clicking to select next slot timings in Reschedule Popup", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(Login_Digitizer.driver, logger), "Clicking on Ok in Popup", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		//Cancelling Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		n=n+1;
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 2, Login_Digitizer.driver, logger), "Clicking on Reschedule in Dropdown", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(Login_Digitizer.driver, logger), "Clicking to get reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(Login_Digitizer.driver, logger),"Clicking on reasons in Cancel Alert", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(Login_Digitizer.driver, logger), "Clicking on Submit in Alert", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
		Web_GeneralFunctions.click(CPage.getPoweredByLink(Login_Digitizer.driver, logger), "Clicking on Dropdown in Booked Appointment", Login_Digitizer.driver, logger);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public synchronized void ScrollAndBookAllSlots(String prevSlotId,String slotId) throws Exception {
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		Thread.sleep(3000);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, Login_Digitizer.driver, logger), Login_Digitizer.driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, Login_Digitizer.driver, logger), "Clicking on Plus Icon", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, Login_Digitizer.driver, logger), "8553406065", "sending value in search box", Login_Digitizer.driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(Login_Digitizer.driver, logger), "Clicking on first available consumer", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(Login_Digitizer.driver, logger), "EMR Automation Testing", "Setting Visit Reason", Login_Digitizer.driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(Login_Digitizer.driver, logger), "Clicking to Submit Appointment", Login_Digitizer.driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	public synchronized void appCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getAppointmentCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	
	public synchronized void CheinCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void CompletedCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(Login_Digitizer.driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void ConsultPopupCheck() throws Exception {
		
		
		CommonPage CPage = new CommonPage();
		try {
			if(CPage.getConsultationAlert(Login_Digitizer.driver, logger,10).isDisplayed()) {
				Web_GeneralFunctions.click(CPage.getConsultationAlert(Login_Digitizer.driver, logger), "Clicking to Checkout and start new Appointment", Login_Digitizer.driver, logger);
			}
		}catch (Exception e) {
			
		}
		
	}
	
	
	public synchronized void NoDiagnosisCheck() throws Exception {
		
		
		ConsultationPage ConsPage = new ConsultationPage();
		
		if(ConsPage.getYesinSweetAlert(Login_Digitizer.driver, logger).isDisplayed()) {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(Login_Digitizer.driver, logger),"Clicking to Checkout and start new Appointment", Login_Digitizer.driver, logger);
		}
		
	}
	
	
	
			
}