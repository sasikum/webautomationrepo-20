package com.tatahealth.EMR.Scripts.UserProfile;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class AddMyExperiences extends UserProfile {

	public static WebDriver driver;

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		// Login_Doctor.LoginTestwithDiffrentUser("SonalinQAEMROne");
		Login_Doctor.LoginTestwithDiffrentUser("DontBooKApptsFromThisDoc"); // staging

		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "AddMyExperiences " })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	CommonsPage commonPage = new CommonsPage();
	SweetAlertPage swaPage = new SweetAlertPage();

	public synchronized void clickOnMyExperiencesLink() {
		try {
			Web_GeneralFunctions.click(getMyExperienceslink(Login_Doctor.driver, logger),
					"Clicked on MyExperiences  link", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnAddExperiencesBtnInExperiences() {
		try {
			Web_GeneralFunctions.click(getAddExperienceBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Experiences ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnSaveBtnInExperiences() {
		try {
			Web_GeneralFunctions.click(getExpSaveBtn(Login_Doctor.driver, logger),
					"Clicked on save button in Experiences ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnEditExperiences() {
		try {
			Web_GeneralFunctions.click(getExpEditBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Experiences ", Login_Doctor.driver, logger);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void clickOnDeletExperiences() {
		try {
			Web_GeneralFunctions.click(getPubDeleteBtn(Login_Doctor.driver, logger),
					"Clicked on edit button in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(2);
			Web_GeneralFunctions.click(getYesBtnOnAlt(Login_Doctor.driver, logger), "Clicked on yes button in  alrt ",
					Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public synchronized void fillExperiences(String organizationName, String role, String fromDate, String toDate,
			String remarks) {
		try {
			Web_GeneralFunctions.click(getExpOrganisationTbx(Login_Doctor.driver, logger),
					"Clicked on Organization Name tbx in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getExpOrganisationTbx(Login_Doctor.driver, logger), organizationName,
					"filling Topic Name", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getExpRoleTbx(Login_Doctor.driver, logger),
					"Clicked on  Role tbx in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getExpRoleTbx(Login_Doctor.driver, logger), role, "filling Role",
					Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getExpFromDateTbx(Login_Doctor.driver, logger),
					"Clicked on From Date tbx in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getExpFromDateTbx(Login_Doctor.driver, logger), fromDate,
					"filling From Date Date in *24-04-2020* formate", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getExpToDateTbx(Login_Doctor.driver, logger),
					"Clicked on toDate tbx in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getExpToDateTbx(Login_Doctor.driver, logger), toDate,
					"filling toDate Date in *24-04-2020* formate", Login_Doctor.driver, logger);

			Web_GeneralFunctions.click(getExpRemarksTbx(Login_Doctor.driver, logger),
					"Clicked on Abstract tbx in Experiences ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(getExpRemarksTbx(Login_Doctor.driver, logger), remarks,
					"filling Remarks Tbx Name", Login_Doctor.driver, logger);

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Test(priority = 1015, groups = { "Regression", "AddMyExperiences" })

	public synchronized void saveExperiencesWithOutData() throws Exception {

		logger = Reports.extent.createTest("AddMyExperiences");
		Web_GeneralFunctions.wait(1);
		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		// Web_GeneralFunctions.waitForElement(driver,
		// getMyExperienceslink(Login_Doctor.driver, logger));
		clickOnMyExperiencesLink();
		clickOnAddExperiencesBtnInExperiences();
		clickOnSaveBtnInExperiences();
		// validated error message below
	}

	@Test(priority = 1016, groups = { "Regression", "AddMyExperiences" })

	public synchronized void saveExperiencesWithData() throws Exception {

		logger = Reports.extent.createTest("saveExperiencesWithData");
		Web_GeneralFunctions.wait(3);
		fillExperiences("tata", "SP", "", "", "");
		clickOnSaveBtnInExperiences();
		Web_GeneralFunctions.wait(1);

	}

	@Test(priority = 1017, groups = { "Regression", "AddMyExperiences" })

	public synchronized void EditExperiences() throws Exception {

		logger = Reports.extent.createTest("EditExperiences");
		Web_GeneralFunctions.wait(3);
		clickOnEditExperiences();

		fillExperiences(" upadate", "upaste", "", "", "llaallaal");
		clickOnSaveBtnInExperiences();
		Web_GeneralFunctions.wait(3);

		// Validate error message
	}

	@Test(priority = 1018, groups = { "Regression", "AddMyExperiences" })

	public synchronized void DeleteExperiences() throws Exception {

		System.out.println("...............................................................");
		logger = Reports.extent.createTest("DeleteExperiences");
		Web_GeneralFunctions.wait(3);
		clickOnDeletExperiences();

		System.out.println("...............................................................");

		// Validate error message
	}

}
