package com.tatahealth.EMR.Scripts.PatientAdvanceSearch;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.tatahealth.EMR.pages.PatientAdvanceSearch.AdvancePatientSearch;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.aventstack.extentreports.ExtentTest;

import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;

public class PatientAdvanceSearch1 {

	public static ExtentTest logger;
	AdvancePatientSearch adPatSearch = new AdvancePatientSearch();

	@BeforeClass(groups= {"Regression","PatientAdvanceSearch"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Patient Advance Search";
		Login_Doctor.LoginTestwithDiffrentUser(role);
	}

	@AfterClass(groups= {"Regression","PatientAdvanceSearch"})
	public static void afterClass() {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}


	@Test(dataProvider = "Module Names Data",groups= {"Regression","PatientAdvanceSearch"})
	public synchronized void advanceSearch02(String moduleName) throws Exception {

		logger = Reports.extent.createTest("Verify Advance Search Page in All Pages");
		Web_GeneralFunctions.wait(6);

		//click left side menu
		Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger), "Click Left main menu ",
				Login_Doctor.driver, logger);
		WebElement element = adPatSearch.getModuleFromLeftMenu(Login_Doctor.driver, logger, moduleName);
		Web_GeneralFunctions.click(element, "Click on " + element.getText() + " menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(adPatSearch.getEmrMenu(Login_Doctor.driver, logger),"Click Left main menu to close the menu", Login_Doctor.driver, logger);

		Web_GeneralFunctions.wait(6);
		AdvancePatientSearch.getAdvanceSearchBtn(Login_Doctor.driver, logger).isDisplayed();
	}

	@DataProvider(name = "Module Names Data")
	public synchronized Object[] getModuleNames() {
		Object[] s = new Object[9];
		s[0] = "User Administration";
		s[1] = "Master Administration";
		s[2] = "Billing";
		s[3] = "Reports";
		s[4] = "Practice Management";
		s[5] = "Medical Certificates";
		s[6] = "Open Consultations";
		s[7] = "Reports View/Upload";
		s[8] = "Appointments";
		return s;
	}
	
}
