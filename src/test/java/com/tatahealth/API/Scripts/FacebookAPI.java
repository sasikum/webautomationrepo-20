package com.tatahealth.API.Scripts;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import com.aventstack.extentreports.model.Log;
public class FacebookAPI {
    File f=new File("fb_Testuser.txt");
    private List<NameValuePair> param;
    //private static String personalAccessToken = "190114671779603|VSrqWhkpSJrSLuGohInYp21ZIEg";
    //private static String orgAccessToken = "436455406691215|1IVfxY-1-Gq6yFbNFDZFXhwILiM";
    //private static String personalAccessToken = "103883554723170|VSrqWhkpSJrSLuGohInYp21ZIEg";
    private static String personalAccessToken = "114316050374281|VSrqWhkpSJrSLuGohInYp21ZIEg";
    private static String orgAccessToken = "436455406691215|1IVfxY-1-Gq6yFbNFDZFXhwILiM";
    
    
    /*public String getPersonalAccessToken() throws Exception {
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.getRequest(PersonalURL);
        return resp.getString("access_token");
    }
    
    
    public String getOrganizationAccessToken() throws Exception {
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.getRequest(OrganizationURL);
        return resp.getString("access_token");
    }*/
    
    
    
    public JSONObject createPersonalUser() throws Exception {
        
        param = new ArrayList<NameValuePair>();
        String app_id = orgAccessToken.substring(0, orgAccessToken.indexOf('|'));
        param.add(new BasicNameValuePair("access_token", orgAccessToken));
        param.add(new BasicNameValuePair("locale", "en_US"));
        param.add(new BasicNameValuePair("name", "Test"));
        String URL = "https://graph.facebook.com/"+app_id+"/accounts/test-users?";
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.PostRequest(URL, param);
        return resp;
    }
    
    
    public JSONObject linkUserToOrgAccount(String uid) throws Exception {
        param = new ArrayList<NameValuePair>();
        String app_id = orgAccessToken.substring(0, orgAccessToken.indexOf('|'));
        param.add(new BasicNameValuePair("access_token", orgAccessToken));
        param.add(new BasicNameValuePair("installed", "true"));
        param.add(new BasicNameValuePair("permissions", "public_profile"));
        param.add(new BasicNameValuePair("uid", uid));
        param.add(new BasicNameValuePair("owner_access_token",orgAccessToken));
        String URL = "https://graph.facebook.com/"+app_id+"/accounts/test-users?";
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.PostRequest(URL, param);
        System.out.println("done linking");
        System.out.println(resp);
        System.out.println();
        return resp;
        
    }
    
    
    public JSONObject deLinkUserToOrgAccount(String uid,String app_access_token) throws Exception {
        param = new ArrayList<NameValuePair>();
        String app_id = orgAccessToken.substring(0, orgAccessToken.indexOf('|'));
        String URL = "https://graph.facebook.com/"+app_id+"/accounts/test-users?access_token="+URLEncoder.encode(orgAccessToken, "UTF-8")+"&uid="+uid+"&app_access_token="+app_access_token;
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.deleteRequest(URL);
        System.out.println("done delinking");
        System.out.println(resp);
        return resp;
        
    }
    
    
    public JSONObject deleteUser(String uid,String app_access_token) throws Exception {
        param = new ArrayList<NameValuePair>();
        String URL = "https://graph.facebook.com/"+uid+"?access_token="+app_access_token;
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        JSONObject resp = API.deleteRequest(URL);
        System.out.println("done Delete User");
        System.out.println(resp);
        return resp;
        
    }
    public void checkdeleteUser() throws Exception {
        if(!f.exists())
        {
            f.createNewFile();
        }
        BufferedReader read = new BufferedReader (new FileReader(f));
        String line=read.readLine();
        while(line!=null)
        {
          if(line.contains("id:"))
          {
              String id=line.replace("id:","").trim();
              String token=read.readLine().replace("access_token:","").trim();
              deleteUser(id,token);
          }
          line=read.readLine();
          }
        read.close();
    }
    public void logFile(JSONObject resp) throws Exception {
        PrintWriter logFile = new PrintWriter(new FileWriter("fb_Testuser.txt",true));
        String email = resp.getString("email");
        String password = resp.getString("password");
        String access_token = resp.getString("access_token");
        String id = resp.getString("id");
        logFile.println("----------------FB USER-------------------");
        logFile.println("email:"+email);
        logFile.println("password:"+password);
        logFile.println("id:"+id);
        logFile.println("access_token:"+access_token);
        logFile.flush();
        logFile.close();
        }
    public void createDeleteFile() throws Exception {
        if(f.exists())
        {
            f.delete();
            f.createNewFile();
        }
        else
        {
            f.createNewFile();
        }
        
    }
    
    
    
    public static void main(String[] args) throws Exception {
        GeneralAPIFunctions API = new GeneralAPIFunctions();
        FacebookAPI API2 = new FacebookAPI();
        API2.checkdeleteUser();
        API2.createDeleteFile();
        int n=16;
        for (int i=1;i<=n;i++){
        JSONObject resp = API2.createPersonalUser();
        API2.logFile(resp);
        String id = resp.getString("id");
        resp = API2.linkUserToOrgAccount(id);   
        }
        }
}
